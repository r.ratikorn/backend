'use strict';

/**
 * product controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::product.product', ({ strapi }) => ({
    // Method 1: Creating an entirely custom action
    async getProduct(ctx) {
         // some custom logic here
        ctx.query = { ...ctx.query, local: 'en' };

        // Calling the default core action
        const { data, meta } = await super.find(ctx);

        // some more custom logic
        meta.date = Date.now();

        return { data, meta }
    },
    
    async postProduct(ctx) {
        // some logic here
        //const response = await super.create(ctx);
        // some more logic

        console.log(ctx.request.body)

        return {hello: "hey"}
   },
}));
