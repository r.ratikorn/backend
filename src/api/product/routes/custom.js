module.exports = {
    routes: [
        {
            method: 'GET',
            path: '/get_product',
            handler: 'product.getProduct',
            config: {
            auth: false,
            }
        },
        {
            method: 'POST',
            path: '/post_product',
            handler: 'product.postProduct',
            config: {
            auth: false,
            }
        }
    ]
}